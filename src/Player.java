
public class Player {

	char name;
	int win;
	int draw;
	int lose;

	public Player(char name) {
		this.name = name;
		win = 0;
		draw = 0;
		lose = 0;
	}

	public char getName() {
		return name;
	}

	public int getWin() {
		return win;
	}

	public int getLose() {
		return lose;
	}

	public int getDraw() {
		return draw;
	}

	public void win() {
		win++;
	}

	public void lose() {
		lose++;
	}

	public void draw() {
		draw++;
	}

}
